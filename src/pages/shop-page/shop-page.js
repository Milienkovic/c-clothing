import React, {Component} from 'react';
import CollectionsOverview from "../../components/collections-overview/collections-overview";
import {Route} from "react-router-dom";

import CollectionPageContainer from "../collection/collection.container";

import {connect} from "react-redux";
import {fetchCollectionsStartAsync} from "../../redux/shop/shop.actions";
import WithSpinner from "../../components/hoc/with-spinner/with-spinner";
import {createStructuredSelector} from "reselect";
import { selectIsCollectionLoaded} from "../../redux/shop/shop.selector";
import CollectionOverviewContainer from "../../components/collections-overview/collection-overview.container";


class ShopPage extends Component {


    componentDidMount() {
       const {fetchCollectionsStartAsync} = this.props;
       fetchCollectionsStartAsync();
    }

    render() {
        const {match} = this.props;
        return (
            <div className='shop-page'>
                <Route exact path={`${match.path}`}
                       component={CollectionOverviewContainer}/>
                <Route path={`${match.path}/:collectionId`}
                       component={CollectionPageContainer}/>
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => ({
    fetchCollectionsStartAsync: () => dispatch(fetchCollectionsStartAsync())
});

export default connect(null, mapDispatchToProps)(ShopPage);