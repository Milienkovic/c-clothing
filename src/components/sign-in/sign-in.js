import React, {Component} from 'react';
import FormInput from "../form-input/form-input";
import Button from "../button/button";
import {signInWithGoogle, auth} from "../../firebase/firebase.utils";

import './sign.in.styles.scss';

class SignIn extends Component {

    state = {
        email: '',
        password: ''
    }

    submitHandler = async event => {
        event.preventDefault();

        const {email, password} = this.state;

        try {
            await auth.signInWithEmailAndPassword(email, password);
            this.setState({email: '', password: ''});
        } catch (e) {
            console.log(e.message);
        }

    }

    handleChange = event => {
        const {value, name} = event.target;
        this.setState({[name]: value})
    }

    render() {
        return (
            <div className='sign-in'>
                <h2>I already have an account</h2>
                <span>Sign in with your email and password</span>

                <form onSubmit={this.submitHandler}>

                    <FormInput name='email'
                               type='email'
                               value={this.state.email}
                               required
                               label='email'
                               handleChange={this.handleChange}
                    />
                    <FormInput name='password'
                               type='password'
                               value={this.state.password}
                               label='password'
                               required
                               handleChange={this.handleChange}/>
                    <div className='buttons'>
                        <Button type='submit'>Sign in</Button>
                        <Button onClick={signInWithGoogle}
                                isGoogleSignIn>Sign in with Google</Button>
                    </div>
                </form>
            </div>
        )
    }

}

export default SignIn;