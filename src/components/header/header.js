import React from 'react';
import './header.styles.scss';
import {createStructuredSelector} from "reselect";

import {ReactComponent as Logo} from '../../assets/4.3 crown.svg.svg';
import {auth} from '../../firebase/firebase.utils';
import {connect} from "react-redux";
import CartIcon from "../cart/cart-icon/cart-icon";
import CartDropdown from "../cart/cart-dropdown/cart-drpodown";
import {selectCurrentUser} from "../../redux/user/user.selector";
import {selectCartHidden} from "../../redux/cart/cart.selectors";

import {HeaderContainer,LogoContainer,OptionsContainer,OptionDiv,OptionLink} from "./header.styles";

const Header = ({currentUser, hidden}) => (
    <HeaderContainer>
        <LogoContainer to='/'>
            <Logo className='logo'/>
        </LogoContainer>
        <OptionsContainer >
            <OptionLink to='/shop' >SHOP</OptionLink>
            <OptionLink to='/contact' >CONTACT</OptionLink>
            {
                currentUser ?
                    (<OptionDiv onClick={() => auth.signOut()}>
                        SIGN OUT
                    </OptionDiv>)
                    :
                    (<OptionLink to='/signin'>SIGN IN</OptionLink>)
            }
            <CartIcon/>
        </OptionsContainer>
        {
            hidden ? null : <CartDropdown/>
        }

    </HeaderContainer>
);

const mapStateToProps = createStructuredSelector({
    currentUser:selectCurrentUser,
    hidden:selectCartHidden
});
export default connect(mapStateToProps)(Header);
