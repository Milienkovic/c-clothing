import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyB5shGYZHTkaz-xXDx4WX0FxnIQIHw4YS4",
    authDomain: "c-clothing-cdf69.firebaseapp.com",
    databaseURL: "https://c-clothing-cdf69.firebaseio.com",
    projectId: "c-clothing-cdf69",
    storageBucket: "",
    messagingSenderId: "847344222032",
    appId: "1:847344222032:web:8e6cfc7d214660c1"
};


export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);

    const collectionRef = firestore.collection('users');

    const snapShot = await userRef.get();
    const collectionShapshot = await collectionRef.get();
    // console.log({collection: collectionShapshot.docs.map(doc => doc.data())});

    if (!snapShot.exists) {
        const {displayName, email} = userAuth;
        const createdAt = new Date();
        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (e) {
            console.log('error ' + e.message);
        }
    }
    return userRef;
};

export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) => {
    const collectionRef = firestore.collection(collectionKey);
    // console.log(collectionRef);

    const batch = firestore.batch();
    objectsToAdd.forEach(obj =>{
        const newDocRef = collectionRef.doc();
        batch.set(newDocRef,obj);
        // console.log(newDocRef);
    });

    return await batch.commit();
};

export const convertCollectionsShampshotToMap=(collectionsSnapshot) =>{
    const transformedCollection = collectionsSnapshot.docs.map(doc =>{
        const {title,items}=doc.data();

        return{
            routeName:encodeURI(title.toLowerCase()),
            id: doc.id,
            title,
            items
        }
    });

    // console.log(transformedCollection)
    return transformedCollection.reduce((accumulator, collection)=>{
        accumulator[collection.title.toLowerCase()]=collection;
        return accumulator;
    },{});
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({promt: 'select_account'});

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
